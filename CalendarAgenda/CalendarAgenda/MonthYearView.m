//
//  MonthYearView.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "MonthYearView.h"

#import "CalendarHandler.h"

typedef NS_ENUM(NSInteger, CalendarContentLayout) {
    CalendarContentLayoutFull,
    CalendarContentLayoutCenter,
    CalendarContentLayoutCenterLeft,
    CalendarContentLayoutCenterRight
};

@interface MonthYearView (){
    CGSize _lastSize;
    
    UIView *_leftView;
    UIView *_centerView;
    UIView *_rightView;
    
    CalendarContentLayout _calViewMode;
}

@end

@implementation MonthYearView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(!self){
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(!self){
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

// For overriding
- (void)commonInit
{
    self.clipsToBounds = YES;
    
    _contentRatio = 1.;
    
    {
        _scrollView = [UIScrollView new];
        [self addSubview:_scrollView];
        
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
        
        _scrollView.clipsToBounds = NO;
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self resizeViewsIfWidthChanged];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(_scrollView.contentSize.width <= 0){
        return;
    }
    
    [_calendarHandler.scrollManager modifyContentOffset:(_scrollView.contentOffset.x / _scrollView.contentSize.width)];
}

- (void)resizeViewsIfWidthChanged
{
    CGSize size = self.frame.size;
    if(size.width != _lastSize.width){
        _lastSize = size;
        
        [self repositionViews];
    }
    else if(size.height != _lastSize.height){
        _lastSize = size;
        
        _scrollView.frame = CGRectMake(_scrollView.frame.origin.x, 0, _scrollView.frame.size.width, size.height);
        _scrollView.contentSize = CGSizeMake(_scrollView.contentSize.width, size.height);
        
        _leftView.frame = CGRectMake(_leftView.frame.origin.x, 0, _scrollView.frame.size.width, size.height);
        _centerView.frame = CGRectMake(_centerView.frame.origin.x, 0, _scrollView.frame.size.width, size.height);
        _rightView.frame = CGRectMake(_rightView.frame.origin.x, 0, _scrollView.frame.size.width, size.height);
    }
}

- (void)repositionViews
{
    // Avoid vertical scrolling when the view is in a UINavigationController
    _scrollView.contentInset = UIEdgeInsetsZero;
    
    {
        CGFloat width = self.frame.size.width * _contentRatio;
        CGFloat x = (self.frame.size.width - width) / 2.;
        CGFloat height = self.frame.size.height;
        
        _scrollView.frame = CGRectMake(x, 0, width, height);
        _scrollView.contentSize = CGSizeMake(width, height);
    }
    
    CGSize size = _scrollView.frame.size;
    
    switch (_calViewMode) {
        case CalendarContentLayoutFull:
            _scrollView.contentSize = CGSizeMake(size.width * 3, size.height);
            
            _leftView.frame = CGRectMake(0, 0, size.width, size.height);
            _centerView.frame = CGRectMake(size.width, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width * 2, 0, size.width, size.height);
            
            _scrollView.contentOffset = CGPointMake(size.width, 0);
            break;
        case CalendarContentLayoutCenter:
            _scrollView.contentSize = size;
            
            _leftView.frame = CGRectMake(- size.width, 0, size.width, size.height);
            _centerView.frame = CGRectMake(0, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width, 0, size.width, size.height);
            
            _scrollView.contentOffset = CGPointZero;
            break;
        case CalendarContentLayoutCenterLeft:
            _scrollView.contentSize = CGSizeMake(size.width * 2, size.height);
            
            _leftView.frame = CGRectMake(0, 0, size.width, size.height);
            _centerView.frame = CGRectMake(size.width, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width * 2, 0, size.width, size.height);
            
            _scrollView.contentOffset = CGPointMake(size.width, 0);
            break;
        case CalendarContentLayoutCenterRight:
            _scrollView.contentSize = CGSizeMake(size.width * 2, size.height);
            
            _leftView.frame = CGRectMake(- size.width, 0, size.width, size.height);
            _centerView.frame = CGRectMake(0, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width, 0, size.width, size.height);
            
            _scrollView.contentOffset = CGPointZero;
            break;
    }
}

- (void)setPrevDate:(NSDate *)previousDate todayDate:(NSDate *)todayDate nextDate:(NSDate *)nextDate
{
    NSAssert(todayDate != nil, @"todayDate cannot be nil");
    NSAssert(_calendarHandler != nil, @"calendarHandler cannot be nil");
    
    if(!_leftView){
        _leftView = [_calendarHandler.delegateHandler buildMonthYearView];
        [_scrollView addSubview:_leftView];
        
        _centerView = [_calendarHandler.delegateHandler buildMonthYearView];
        [_scrollView addSubview:_centerView];
        
        _rightView = [_calendarHandler.delegateHandler buildMonthYearView];
        [_scrollView addSubview:_rightView];
    }
    
    [_calendarHandler.delegateHandler prepareMonthYearView:_leftView date:previousDate];
    [_calendarHandler.delegateHandler prepareMonthYearView:_centerView date:todayDate];
    [_calendarHandler.delegateHandler prepareMonthYearView:_rightView date:nextDate];
    
    BOOL haveLeftPage = [_calendarHandler.delegateHandler canDisplayPageWithDate:previousDate];
    BOOL haveRightPage = [_calendarHandler.delegateHandler canDisplayPageWithDate:nextDate];
    
    if(_calendarHandler.settings.calViewHideWhenPossible){
        _leftView.hidden = !haveLeftPage;
        _rightView.hidden = !haveRightPage;
    }
    else{
        _leftView.hidden = NO;
        _rightView.hidden = NO;
    }
}

- (void)modifyContentLayout:(NSUInteger)calViewMode
{
    if(_calViewMode == calViewMode){
        return;
    }
    
    _calViewMode = calViewMode;
    [self repositionViews];
}

@end
