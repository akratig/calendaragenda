//
//  CustomBarButtonItem.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 05/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomBarButtonItem : UIBarButtonItem

-(id _Nonnull )initWithImage:(UIImage *_Nonnull)img addTarget:(nullable id)target action:(SEL _Nonnull )action;

@end
