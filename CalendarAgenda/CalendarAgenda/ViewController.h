//
//  ViewController.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 23/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarDelegate.h"
#import "MonthYearView.h"
#import "ContentView.h"
#import "AgendaView.h"

@interface ViewController : UIViewController<CalendarDelegate>{
    MonthYearView *monthYearView;
    ContentView *contentView;
    AgendaView *agendaView;
    NSDictionary *temperatureDataDict;
}

@property (strong, nonatomic) CalendarHandler *calendarHandler;

@end
