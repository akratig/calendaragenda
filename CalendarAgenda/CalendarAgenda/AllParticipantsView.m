//
//  AllParticipantsView.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 07/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "AllParticipantsView.h"
#import "Constants.h"
#import "ParticipantView.h"
#import "ParticipantData.h"
#import "CommonMethods.h"

@implementation AllParticipantsView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)updateWithParticipants:(NSMutableArray *)allParticipantsArray{
    for (int i=0; i<[allParticipantsArray count]; i++) {
        ParticipantView *participantView = [[ParticipantView alloc] initWithFrame:CGRectMake(i*(VIEW_SIZE+X_MARGIN*4), Y_MARGIN, VIEW_SIZE, VIEW_SIZE)];
        
        ParticipantData *participantData = [allParticipantsArray objectAtIndex:i];
        
        //Set Participant Name Label
        [[participantView participantNameLabel] setText:participantData.userNameInitialsString];
        [[participantView participantNameLabel] setFrame:CGRectMake(0, Y_MARGIN, participantView.frame.size.width, LABEL_HEIGHT)];
        
        //if user image os not available, then set a random background color
        if (participantData.userImage==nil) {
            [[participantView participantLayoutView] setBackgroundColor:[CommonMethods findColorWithString:participantData.userBackgroundColor]];
        }
        else{
            [[participantView participantLayoutView] setImage:participantData.userImage];
        }
        
        switch (participantData.userStatus) {
            case AVAILABLE:
                [[participantView availabilityStatusView] setBackgroundColor:[UIColor greenColor]];
                break;
            case BUSY:
                [[participantView availabilityStatusView] setBackgroundColor:[UIColor redColor]];
                break;
            case IDLE:
                [[participantView availabilityStatusView] setBackgroundColor:[UIColor yellowColor]];
                break;
            case OFFLINE:
                [[participantView availabilityStatusView] setBackgroundColor:[UIColor lightGrayColor]];
                break;
                
            default:
                break;
        }
        
        [self addSubview:participantView];
        [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, (VIEW_SIZE+X_MARGIN*4)*(i+1), VIEW_SIZE)];
    }
    return self;
}

@end
