//
//  CalendarConfiguration.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalendarConfiguration : NSObject

// Content view

@property (nonatomic) BOOL calViewHideWhenPossible;

// Calendar view

// Must be less or equalt to 6, 0 for automatic
@property (nonatomic) NSUInteger calViewNumberOfWeeks;
@property (nonatomic) BOOL calViewHaveWeekDaysView;
@property (nonatomic) NSUInteger calViewWeekModeNumberOfWeeks;
@property (nonatomic) BOOL calViewWeekDaysViewAutomaticHeight;

- (void)commonInit;

@end
