//
//  ContentView.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "ContentView.h"

#import "CalendarHandler.h"

typedef NS_ENUM(NSInteger, CalendarContentLayout) {
    CalendarContentLayoutFull,
    CalendarContentLayoutCenter,
    CalendarContentLayoutCenterLeft,
    CalendarContentLayoutCenterRight
};

@interface ContentView (){
    CGSize _lastSize;
    
    UIView<CalendarViewObject> *_leftView;
    UIView<CalendarViewObject> *_centerView;
    UIView<CalendarViewObject> *_rightView;
    
    CalendarContentLayout _calViewMode;
}

@end

@implementation ContentView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(!self){
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(!self){
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

// For overriding
- (void)commonInit
{
    self.showsHorizontalScrollIndicator = NO;
    self.showsVerticalScrollIndicator = NO;
    self.pagingEnabled = YES;
    self.clipsToBounds = YES;
}

- (void)layoutSubviews
{
    [self resizeViewsIfWidthChanged];
    [self viewDidScroll];
}

- (void)resizeViewsIfWidthChanged
{
    CGSize size = self.frame.size;
    if(size.width != _lastSize.width){
        _lastSize = size;
        
        [self repositionViews];
    }
    else if(size.height != _lastSize.height){
        _lastSize = size;
        
        _leftView.frame = CGRectMake(_leftView.frame.origin.x, 0, size.width, size.height);
        _centerView.frame = CGRectMake(_centerView.frame.origin.x, 0, size.width, size.height);
        _rightView.frame = CGRectMake(_rightView.frame.origin.x, 0, size.width, size.height);
        
        self.contentSize = CGSizeMake(self.contentSize.width, size.height);
    }
}

- (void)viewDidScroll
{
    if(self.contentSize.width <= 0){
        return;
    }

    CGSize size = self.frame.size;
    
    switch (_calViewMode) {
        case CalendarContentLayoutFull:
            
            if(self.contentOffset.x < size.width / 2.){
                [self loadPrevPage];
            }
            else if(self.contentOffset.x > size.width * 1.5){
                [self loadNextPage];
            }
            
            break;
        case CalendarContentLayoutCenter:
            
            break;
        case CalendarContentLayoutCenterLeft:
            
            if(self.contentOffset.x < size.width / 2.){
                [self loadPrevPage];
            }
            
            break;
        case CalendarContentLayoutCenterRight:
            
            if(self.contentOffset.x > size.width / 2.){
                [self loadNextPage];
            }
            
            break;
    }
    
    [_calendarHandler.scrollManager modifyMenuContentOffset:(self.contentOffset.x / self.contentSize.width) calViewMode:_calViewMode];
}

- (void)loadPrevPageWithAnimation
{
    switch (_calViewMode) {
        case CalendarContentLayoutCenterRight:
        case CalendarContentLayoutCenter:
            return;
        default:
            break;
    }
    
    CGSize size = self.frame.size;
    CGPoint point = CGPointMake(self.contentOffset.x - size.width, 0);
    [self setContentOffset:point animated:YES];
}

- (void)loadNextPageWithAnimation
{
    switch (_calViewMode) {
        case CalendarContentLayoutCenterLeft:
        case CalendarContentLayoutCenter:
            return;
        default:
            break;
    }
    
    CGSize size = self.frame.size;
    CGPoint point = CGPointMake(self.contentOffset.x + size.width, 0);
    [self setContentOffset:point animated:YES];
}

- (void)loadPrevPage
{
    NSDate *nextDate = [_calendarHandler.delegateHandler dateForPrevPageWithCurrentDate:_leftView.date];
    
    // Must be set before chaging date for LayoutView for updating day views
    self->_date = _leftView.date;
    
    UIView<CalendarViewObject> *tmpView = _rightView;
    
    _rightView = _centerView;
    _centerView = _leftView;
    
    _leftView = tmpView;
    _leftView.date = nextDate;
    
    [self modifyMenuDates];
    
    CalendarContentLayout previousContentLayout = _calViewMode;
    
    [self modifyContentLayout];
    
    CGSize size = self.frame.size;
    
    switch (_calViewMode) {
        case CalendarContentLayoutFull:
            
            _leftView.frame = CGRectMake(0, 0, size.width, size.height);
            _centerView.frame = CGRectMake(size.width, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width * 2, 0, size.width, size.height);
            
            if(previousContentLayout == CalendarContentLayoutFull){
                self.contentOffset = CGPointMake(self.contentOffset.x + size.width, 0);
            }
            else if(previousContentLayout ==  CalendarContentLayoutCenterLeft){
                self.contentOffset = CGPointMake(self.contentOffset.x + size.width, 0);
            }
            
            self.contentSize = CGSizeMake(size.width * 3, size.height);
            
            break;
        case CalendarContentLayoutCenter:
            // Not tested
            
            _leftView.frame = CGRectMake(- size.width, 0, size.width, size.height);
            _centerView.frame = CGRectMake(0, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width, 0, size.width, size.height);
            
            self.contentSize = size;
            
            break;
        case CalendarContentLayoutCenterLeft:
            
            _leftView.frame = CGRectMake(0, 0, size.width, size.height);
            _centerView.frame = CGRectMake(size.width, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width * 2, 0, size.width, size.height);
            
            self.contentOffset = CGPointMake(self.contentOffset.x + size.width, 0);
            self.contentSize = CGSizeMake(size.width * 2, size.height);
            
            break;
        case CalendarContentLayoutCenterRight:
            
            _leftView.frame = CGRectMake(- size.width, 0, size.width, size.height);
            _centerView.frame = CGRectMake(0, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width, 0, size.width, size.height);
            
            self.contentSize = CGSizeMake(size.width * 2, size.height);
            
            break;
    }
    
    // Update dayViews becuase current month changed
    [_rightView reload];
    [_centerView reload];
    
    if(_calendarHandler.delegate && [_calendarHandler.delegate respondsToSelector:@selector(previousPageLoaded:)]){
        [_calendarHandler.delegate previousPageLoaded:_calendarHandler];
    }
}

- (void)loadNextPage
{
    NSDate *nextDate = [_calendarHandler.delegateHandler dateForNextPageWithCurrentDate:_rightView.date];
    
    // Must be set before chaging date for LayoutView for updating day views
    self->_date = _rightView.date;
    
    UIView<CalendarViewObject> *tmpView = _leftView;
    
    _leftView = _centerView;
    _centerView = _rightView;
    
    _rightView = tmpView;
    _rightView.date = nextDate;
    
    [self modifyMenuDates];
    
    CalendarContentLayout previousContentLayout = _calViewMode;
    
    [self modifyContentLayout];
    
    CGSize size = self.frame.size;
    
    switch (_calViewMode) {
        case CalendarContentLayoutFull:
            
            _leftView.frame = CGRectMake(0, 0, size.width, size.height);
            _centerView.frame = CGRectMake(size.width, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width * 2, 0, size.width, size.height);
            
            if(previousContentLayout == CalendarContentLayoutFull){
                self.contentOffset = CGPointMake(self.contentOffset.x - size.width, 0);
            }
            self.contentSize = CGSizeMake(size.width * 3, size.height);
            
            break;
        case CalendarContentLayoutCenter:
            // Not tested
            
            _leftView.frame = CGRectMake(- size.width, 0, size.width, size.height);
            _centerView.frame = CGRectMake(0, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width, 0, size.width, size.height);
            
            self.contentSize = size;
            
            break;
        case CalendarContentLayoutCenterLeft:
            
            _leftView.frame = CGRectMake(0, 0, size.width, size.height);
            _centerView.frame = CGRectMake(size.width, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width * 2, 0, size.width, size.height);

            if(previousContentLayout != CalendarContentLayoutCenterRight){
                self.contentOffset = CGPointMake(self.contentOffset.x - size.width, 0);
            }

            // Must be set a the end else the scroll freeze
            self.contentSize = CGSizeMake(size.width * 2, size.height);

            break;
        case CalendarContentLayoutCenterRight:
            // Not tested
            
            _leftView.frame = CGRectMake(- size.width, 0, size.width, size.height);
            _centerView.frame = CGRectMake(0, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width, 0, size.width, size.height);

            self.contentSize = CGSizeMake(size.width * 2, size.height);
            
            break;
    }
    
    // Update dayViews becuase current month changed
    [_leftView reload];
    [_centerView reload];
    
    if(_calendarHandler.delegate && [_calendarHandler.delegate respondsToSelector:@selector(nextPageLoaded:)]){
        [_calendarHandler.delegate nextPageLoaded:_calendarHandler];
    }
}

- (void)setDate:(NSDate *)date
{
    NSAssert(date != nil, @"date cannot be nil");
    NSAssert(_calendarHandler != nil, @"calendarHandler cannot be nil");
    
    self->_date = date;
    
    if(!_leftView){
        _leftView = [_calendarHandler.delegateHandler buildLayoutView];
        [self addSubview:_leftView];
        
        _centerView = [_calendarHandler.delegateHandler buildLayoutView];
        [self addSubview:_centerView];
        
        _rightView = [_calendarHandler.delegateHandler buildLayoutView];
        [self addSubview:_rightView];
        
        [self modifyManagerForViews];
    }
    
    _leftView.date = [_calendarHandler.delegateHandler dateForPrevPageWithCurrentDate:date];
    _centerView.date = date;
    _rightView.date = [_calendarHandler.delegateHandler dateForNextPageWithCurrentDate:date];
    
    [self modifyMenuDates];
    
    [self modifyContentLayout];
    [self repositionViews];
}

- (void)setCalendarHandler:(CalendarHandler *)calendarHandler
{
    self->_calendarHandler = calendarHandler;
    [self modifyManagerForViews];
}

- (void)modifyManagerForViews
{
    if(!_calendarHandler || !_leftView){
        return;
    }
    
    _leftView.calendarHandler = _calendarHandler;
    _centerView.calendarHandler = _calendarHandler;
    _rightView.calendarHandler = _calendarHandler;
}

- (void)modifyContentLayout
{
    BOOL haveLeftPage = [_calendarHandler.delegateHandler canDisplayPageWithDate:_leftView.date];
    BOOL haveRightPage = [_calendarHandler.delegateHandler canDisplayPageWithDate:_rightView.date];
    
    if(haveLeftPage && haveRightPage){
        _calViewMode = CalendarContentLayoutFull;
    }
    else if(!haveLeftPage && !haveRightPage){
        _calViewMode = CalendarContentLayoutCenter;
    }
    else if(!haveLeftPage){
        _calViewMode = CalendarContentLayoutCenterRight;
    }
    else{
        _calViewMode = CalendarContentLayoutCenterLeft;
    }
    
    if(_calendarHandler.settings.calViewHideWhenPossible){
        _leftView.hidden = !haveLeftPage;
        _rightView.hidden = !haveRightPage;
    }
    else{
        _leftView.hidden = NO;
        _rightView.hidden = NO;
    }
}

- (void)repositionViews
{
    CGSize size = self.frame.size;
    self.contentInset = UIEdgeInsetsZero;
    
    switch (_calViewMode) {
        case CalendarContentLayoutFull:
            self.contentSize = CGSizeMake(size.width * 3, size.height);
            
            _leftView.frame = CGRectMake(0, 0, size.width, size.height);
            _centerView.frame = CGRectMake(size.width, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width * 2, 0, size.width, size.height);
            
            self.contentOffset = CGPointMake(size.width, 0);
            break;
        case CalendarContentLayoutCenter:
            self.contentSize = size;
            
            _leftView.frame = CGRectMake(- size.width, 0, size.width, size.height);
            _centerView.frame = CGRectMake(0, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width, 0, size.width, size.height);
            
            self.contentOffset = CGPointZero;
            break;
        case CalendarContentLayoutCenterLeft:
            self.contentSize = CGSizeMake(size.width * 2, size.height);
            
            _leftView.frame = CGRectMake(0, 0, size.width, size.height);
            _centerView.frame = CGRectMake(size.width, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width * 2, 0, size.width, size.height);
            
            self.contentOffset = CGPointMake(size.width, 0);
            break;
        case CalendarContentLayoutCenterRight:
            self.contentSize = CGSizeMake(size.width * 2, size.height);
            
            _leftView.frame = CGRectMake(- size.width, 0, size.width, size.height);
            _centerView.frame = CGRectMake(0, 0, size.width, size.height);
            _rightView.frame = CGRectMake(size.width, 0, size.width, size.height);
            
            self.contentOffset = CGPointZero;
            break;
    }
}

- (void)modifyMenuDates
{
    [_calendarHandler.scrollManager setMenuPrevDate:_leftView.date
                                    todayDate:_centerView.date
                                       nextDate:_rightView.date];
}

@end
