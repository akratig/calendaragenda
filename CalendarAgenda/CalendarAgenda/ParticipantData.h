//
//  ParticipantData.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 07/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum UserStatus {
    AVAILABLE,
    BUSY,
    IDLE ,
    OFFLINE
}kStatus;

#define kStatusArray @"AVAILABLE", @"BUSY", @"IDLE", @"OFFLINE", nil

@interface ParticipantData : NSObject

@property (nonatomic, strong) NSString * _Nonnull userNameString;
@property (nonatomic, strong) NSString * _Nonnull userNameInitialsString;
@property (nonatomic,assign)kStatus userStatus;
@property (nonatomic, strong) UIImage * _Nonnull userImage;
@property (nonatomic,strong) NSString * _Nonnull userBackgroundColor;

-(kStatus) statusFromString:(NSString*_Nonnull)userStatusString;
-(id _Nonnull )initWithUserName:(NSString *_Nonnull)userName andUserStatus:(NSString *_Nonnull)userStatus andUserImage:(UIImage *_Nullable)userImage orUserColor:(NSString *_Nonnull)userColor;

@end
