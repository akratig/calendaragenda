//
//  CalendarDelegateHandler.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CalendarDelegate.h"

// Default behavior when no delegate provided

@interface CalendarDelegateHandler : NSObject

@property (nonatomic, weak) CalendarHandler *calendarHandler;

// MonthYear view

- (UIView *)buildMonthYearView;
- (void)prepareMonthYearView:(UIView *)monthYearView date:(NSDate *)date;

// Content view

- (UIView<CalendarViewObject> *)buildLayoutView;

- (BOOL)canDisplayPageWithDate:(NSDate *)todayDate;

- (NSDate *)dateForPrevPageWithCurrentDate:(NSDate *)todayDate;
- (NSDate *)dateForNextPageWithCurrentDate:(NSDate *)todayDate;

// Calendar view

- (UIView<WeekDayViewObject> *)buildWeekDayView;
- (UIView<WeekViewObject> *)buildWeekView;


// Week view

- (UIView<DayViewObject> *)buildDayView;


// Day view

- (void)prepareDayView:(UIView<DayViewObject> *)dayView;
- (void)didTouchDayView:(UIView<DayViewObject> *)dayView;

@end
