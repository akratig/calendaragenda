//
//  CalendarHandler.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "CalendarHandler.h"

#import "ContentView.h"

@implementation CalendarHandler

- (instancetype)init
{
    self = [super init];
    if(!self){
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

// For overriding
- (void)commonInit
{
    _dateTimeManager = [DateTimeManager new];
    _settings = [CalendarConfiguration new];
    
    _delegateHandler = [CalendarDelegateHandler new];
    _delegateHandler.calendarHandler = self;
    
    _scrollManager = [ScrollerHandler new];
    _scrollManager.calendarHandler = self;
}

- (void)setContentView:(UIScrollView<ScrollViewObject> *)contentView
{
    [_contentView setCalendarHandler:nil];
    self->_contentView = contentView;
    [_contentView setCalendarHandler:self];
    
    // For synchronising ContentView
    if([_contentView isKindOfClass:[ContentView class]]){
        _scrollManager.contentView = _contentView;
    }
    else{
        _scrollManager.contentView = nil;
    }
}

- (void)setMonthYearView:(UIScrollView<MonthYearViewObject> *)monthYearView
{
    [_monthYearView setCalendarHandler:nil];
    self->_monthYearView = monthYearView;
    [_monthYearView setCalendarHandler:self];
    
    _scrollManager.monthYearView = _monthYearView;
}

- (NSDate *)date
{
    return _contentView.date;
}

- (void)setDate:(NSDate *)date
{
    [_contentView setDate:date];
}

- (void)reload
{
    [_contentView setDate:_contentView.date];
}

@end
