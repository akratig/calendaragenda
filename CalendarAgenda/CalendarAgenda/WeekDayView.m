//
//  WeekDayView.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "WeekDayView.h"

#import "CalendarHandler.h"

#define NUMBER_OF_DAY_BY_WEEK 7.

@implementation WeekDayView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(!self){
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(!self){
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

// For overriding
- (void)commonInit
{
    NSMutableArray *dayViews = [NSMutableArray new];
    
    for(int i = 0; i < NUMBER_OF_DAY_BY_WEEK; ++i){
        UILabel *label = [UILabel new];
        [self addSubview:label];
        [dayViews addObject:label];
        
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor colorWithRed:152./256. green:147./256. blue:157./256. alpha:1.];
        label.font = [UIFont systemFontOfSize:11];
    }
    
    _dayViews = dayViews;
}

//Rebuilds the view, should be called if `weekDayFormat` or `firstWeekday` is changed
- (void)reload
{
    NSAssert(_calendarHandler != nil, @"calendarHandler cannot be nil");
    
    NSDateFormatter *dateFormatter = [_calendarHandler.dateTimeManager createDateFormatter];
    NSMutableArray *days = nil;
    
    dateFormatter.timeZone = _calendarHandler.dateTimeManager.cal.timeZone;
    dateFormatter.locale = _calendarHandler.dateTimeManager.cal.locale;
    
    days = [[dateFormatter veryShortStandaloneWeekdaySymbols] mutableCopy];

    for(NSInteger i = 0; i < days.count; ++i){
        NSString *day = days[i];
        [days replaceObjectAtIndex:i withObject:[day uppercaseString]];
    }
    
    // Redorder days
    {
        NSCalendar *cal = [_calendarHandler.dateTimeManager cal];
        NSUInteger firstWeekday = (cal.firstWeekday + 6) % 7; // Sunday == 1, Saturday == 7
        
        for(int i = 0; i < firstWeekday; ++i){
            id day = [days firstObject];
            [days removeObjectAtIndex:0];
            [days addObject:day];
        }
    }
    
    for(int i = 0; i < NUMBER_OF_DAY_BY_WEEK; ++i){
        UILabel *label =  _dayViews[i];
        label.text = days[i];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if(!_dayViews){
        return;
    }
    
    CGFloat x = 0;
    CGFloat dayWidth = self.frame.size.width / NUMBER_OF_DAY_BY_WEEK;
    CGFloat dayHeight = self.frame.size.height;
    
    for(UIView *dayView in _dayViews){
        dayView.frame = CGRectMake(x, 0, dayWidth, dayHeight);
        x += dayWidth;
    }
}

@end
