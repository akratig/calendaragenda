//
//  WeekView.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WeekViewObject.h"

@interface WeekView : UIView<WeekViewObject>

@property (nonatomic, weak) CalendarHandler *calendarHandler;

@property (nonatomic, readonly) NSDate *startDate;

- (void)commonInit;

@end
