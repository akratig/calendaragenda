//
//  LayoutView.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "LayoutView.h"

#import "CalendarHandler.h"

#define MAX_WEEKS_BY_MONTH 6

@interface LayoutView (){
    UIView<WeekDayViewObject> *_weekDayView;
    NSMutableArray *_weeksViews;
    NSUInteger _numberOfWeeksDisplayed;
}

@end

@implementation LayoutView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(!self){
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(!self){
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

// For overriding
- (void)commonInit
{
    // May be used in future
}

- (void)setDate:(NSDate *)date
{
    NSAssert(_calendarHandler != nil, @"calendarHandler cannot be nil");
    NSAssert(date != nil, @"date cannot be nil");
    
    self->_date = date;
    
    [self reload];
}

- (void)reload
{
    if(_calendarHandler.settings.calViewHaveWeekDaysView && !_weekDayView){
        _weekDayView = [_calendarHandler.delegateHandler buildWeekDayView];
        [self addSubview:_weekDayView];
    }
    _weekDayView.calendarHandler = _calendarHandler;
    [_weekDayView reload];
    
    if(!_weeksViews){
        _weeksViews = [NSMutableArray new];
        
        for(int i = 0; i < MAX_WEEKS_BY_MONTH; ++i){
            UIView<WeekViewObject> *weekView = [_calendarHandler.delegateHandler buildWeekView];
            [_weeksViews addObject:weekView];
            [self addSubview:weekView];
                        
            weekView.calendarHandler = _calendarHandler;
        }
    }
    
    NSDate *weekDate = nil;
    
    _numberOfWeeksDisplayed = MIN(_calendarHandler.settings.calViewNumberOfWeeks, MAX_WEEKS_BY_MONTH);
    if(_numberOfWeeksDisplayed == 0){
        _numberOfWeeksDisplayed = [_calendarHandler.dateTimeManager numberOfWeeks:_date];
    }
    
    weekDate = [_calendarHandler.dateTimeManager firstWeekDayOfMonth:_date];
    
    for(NSUInteger i = 0; i < _numberOfWeeksDisplayed; i++){
        UIView<WeekViewObject> *weekView = _weeksViews[i];
        
        weekView.hidden = NO;
        
        // Process the check on another month for the 1st, 4th and 5th weeks
        if(i == 0 || i >= 4){
            [weekView setStartDate:weekDate modifyAnotherMonth:YES monthDate:_date];
        }
        else{
            [weekView setStartDate:weekDate modifyAnotherMonth:NO monthDate:_date];
        }
        
        weekDate = [_calendarHandler.dateTimeManager addToDate:weekDate weeks:1];
    }
    
    for(NSUInteger i = _numberOfWeeksDisplayed; i < MAX_WEEKS_BY_MONTH; i++){
        UIView<WeekViewObject> *weekView = _weeksViews[i];
        
        weekView.hidden = YES;
    }
}

- (void)layoutSubviews
{    
    [super layoutSubviews];

    if(!_weeksViews){
        return;
    }
    
    CGFloat y = 0;
    CGFloat weekWidth = self.frame.size.width;
    
    if(_calendarHandler.settings.calViewHaveWeekDaysView){
        CGFloat weekDayHeight = _weekDayView.frame.size.height; // Force use default height
        
        // Or use the same height than weeksViews
        if(weekDayHeight == 0 || _calendarHandler.settings.calViewWeekDaysViewAutomaticHeight){
            weekDayHeight = self.frame.size.height / (_numberOfWeeksDisplayed + 1);
        }
        
        _weekDayView.frame = CGRectMake(0, 0, weekWidth, weekDayHeight);
        y = weekDayHeight;
    }
    
    CGFloat weekHeight = (self.frame.size.height - y) / _numberOfWeeksDisplayed;
    
    for(UIView *weekView in _weeksViews){
        weekView.frame = CGRectMake(0, y, weekWidth, weekHeight);
        y += weekHeight;
    }
}

@end
