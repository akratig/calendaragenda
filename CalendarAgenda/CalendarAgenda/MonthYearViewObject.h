//
//  Menu.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CalendarHandler;

@protocol MonthYearViewObject <NSObject>

- (void)setCalendarHandler:(CalendarHandler *)calendarHandlerHandler;

- (void)setPrevDate:(NSDate *)previousDate
            todayDate:(NSDate *)todayDate
               nextDate:(NSDate *)nextDate;

- (void)modifyContentLayout:(NSUInteger)calViewMode;

- (UIScrollView *)scrollView;

@end
