//
//  WeekDayView.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WeekDayViewObject.h"

@interface WeekDayView : UIView<WeekDayViewObject>

@property (nonatomic, weak) CalendarHandler *calendarHandler;

@property (nonatomic, readonly) NSArray *dayViews;

- (void)commonInit;

- (void)reload;

@end
