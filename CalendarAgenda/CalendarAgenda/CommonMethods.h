//
//  CommonMethods.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 08/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CommonMethods : NSObject

+(UIColor *)findColorWithString:(NSString *)userBackgroundColor;
+(NSString *)getRandomStringFromArray:(NSArray *)array;
+(NSArray *)getRandomArrayFromArray:(NSArray *)array;

@end
