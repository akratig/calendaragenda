//
//  ServerHandler.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 03/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "ServerHandler.h"
#import <CoreLocation/CoreLocation.h>

@implementation ServerHandler

@synthesize state;

- (void)getResponse:(NSData *)responseData {
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    if (self.state == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TemperatureNotification" object:json];
    }
}

-(CLLocationCoordinate2D) getLocation{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    return coordinate;
}

-(NSString *)getDataStringForTemperatureRequest{
    NSString *dataStr;
    CLLocationCoordinate2D coordinate = [self getLocation];
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    NSTimeInterval time = ([[NSDate date] timeIntervalSince1970]); // returned as a double
    long digits = (long)time;
    NSString *timestampString = [NSString stringWithFormat:@"%ld",digits];
    
    //Getting current temperature
    dataStr = [NSString stringWithFormat:@"%@,%@,%@?exclude=flags,minutely,hourly,daily,alerts",latitude,longitude,timestampString];
    return dataStr;
}

-(void)getCurrentTemperature{
    NSString *dataStr = [self getDataStringForTemperatureRequest];
    NSString *urlString = [NSString stringWithFormat:@"%@%s/%@",SERVER_URL,SECRET_KEY,dataStr];
    NSURL *url = [NSURL URLWithString:urlString];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data != nil) {
            [self performSelectorOnMainThread:@selector(getResponse:) withObject:data waitUntilDone:YES];
        }
        else{
            if (self.state == 1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"TemperatureNotification" object:nil];
            }
        }
    }] resume];
}

@end
