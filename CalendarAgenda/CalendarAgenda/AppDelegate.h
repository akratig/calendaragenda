//
//  AppDelegate.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 23/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

