//
//  Constants.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 03/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#endif /* Constants_h */

//EventTableViewCell Constants

#define X_OFFSET 10.0f
#define Y_OFFSET 15.0f
#define TIME_LABEL_WIDTH 60.0f
#define LABEL_HEIGHT 20.0f
#define VIEW_SIZE 24.0f

#define X_MARGIN 2.0f
#define Y_MARGIN 4.0f

//Generic Constants

#define LIGHT_BLUE_COLOR [UIColor colorWithRed:0.96 green:0.98 blue:0.99 alpha:1.0]
#define NORMAL_BLUE_COLOR [UIColor colorWithRed:0.00 green:0.42 blue:0.74 alpha:1.0]
#define GREEN_COLOR [UIColor colorWithRed:0.41 green:0.78 blue:0.56 alpha:1.0]
#define MUSTARD_COLOR [UIColor colorWithRed:0.95 green:0.73 blue:0.16 alpha:1.0]
#define LIGHT_GREEN_COLOR [UIColor colorWithRed:0.67 green:0.83 blue:0.48 alpha:1.0]
#define LIGHT_PURPLE_COLOR [UIColor colorWithRed:0.74 green:0.13 blue:0.87 alpha:1.0]

//UIImage links

#define MENU_IMAGE [UIImage imageNamed:@"menu.png"]
#define OPTIONS_IMAGE [UIImage imageNamed:@"options.png"]
#define ADD_IMAGE [UIImage imageNamed:@"add.png"]
