//
//  EventTableViewCell.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 28/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "EventTableViewCell.h"
#import "Constants.h"

@implementation EventTableViewCell

@synthesize timeLabel = _timeLabel;
@synthesize timeDurationLabel = _timeDurationLabel;
@synthesize eventTypeLayoutView = _eventTypeLayoutView;
@synthesize descriptionLabel = _descriptionLabel;
@synthesize allParticipantsView = _allParticipantsView;
@synthesize meetingPlaceLabel = _meetingPlaceLabel;
@synthesize locationImageView = _locationImageView;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Meeting time
        self.timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(X_OFFSET,Y_OFFSET, TIME_LABEL_WIDTH, LABEL_HEIGHT)];
        self.timeLabel.font = [UIFont systemFontOfSize:12.0f];
        [self addSubview:self.timeLabel];
        
        // Meeting time duration
        self.timeDurationLabel = [[UILabel alloc]initWithFrame:CGRectMake( X_OFFSET, self.timeLabel.frame.origin.y+self.timeLabel.frame.size.height, TIME_LABEL_WIDTH, LABEL_HEIGHT)];
        self.timeDurationLabel.font = [UIFont systemFontOfSize:12.0f];
        [self.timeDurationLabel setTextColor:[UIColor lightGrayColor]];
        [self addSubview:self.timeDurationLabel];
        
        self.eventTypeLayoutView = [[UIView alloc]initWithFrame:CGRectMake(self.timeLabel.frame.origin.x+self.timeLabel.frame.size.width+ X_OFFSET*2, Y_OFFSET+Y_MARGIN, 15, 15)];
        self.eventTypeLayoutView.layer.cornerRadius = self.eventTypeLayoutView.frame.size.width / 2;
        self.eventTypeLayoutView.clipsToBounds = YES;
        [self addSubview:self.eventTypeLayoutView];
        
        //About the event
        self.descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.eventTypeLayoutView.frame.origin.x+self.eventTypeLayoutView.frame.size.width+ X_OFFSET, Y_OFFSET, self.frame.size.width-(self.eventTypeLayoutView.frame.origin.x+self.eventTypeLayoutView.frame.size.width+X_OFFSET), LABEL_HEIGHT)];
        self.descriptionLabel.font = [UIFont systemFontOfSize:15.0f];
        [self addSubview:self.descriptionLabel];
        
        //All Participants View
        _allParticipantsView = [[AllParticipantsView alloc]init];
        [self addSubview:_allParticipantsView];
        
        // Location Icon
        self.locationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_allParticipantsView.frame.origin.x, _allParticipantsView.frame.origin.y+_allParticipantsView.frame.size.height+Y_OFFSET+Y_MARGIN, VIEW_SIZE, VIEW_SIZE)];
        [self.locationImageView setImage:[UIImage imageNamed:@"loc"]];
        [self addSubview:self.locationImageView];
        
        // Meeting Place
        self.meetingPlaceLabel = [[UILabel alloc] init];
        self.meetingPlaceLabel.font = [UIFont systemFontOfSize:14.0f];
        [self.meetingPlaceLabel setTextColor:[UIColor lightGrayColor]];
        [self addSubview:self.meetingPlaceLabel];
        
    }
    return self;
}

@end
