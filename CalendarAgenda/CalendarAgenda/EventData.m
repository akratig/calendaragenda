//
//  EventData.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 02/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "EventData.h"
#import "DateTimeManager.h"
#import "ParticipantData.h"

@implementation EventData

@synthesize eventDate = _eventDate;
@synthesize timeString = _timeString;
@synthesize timeDurationString = _timeDurationString;
@synthesize descriptionString = _descriptionString;
@synthesize participantNameString = _participantNameString;
@synthesize participantsArray = _participantsArray;
@synthesize meetingPlaceString = _meetingPlaceString;

@end
