//
//  ParticipantData.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 07/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "ParticipantData.h"

@implementation ParticipantData

@synthesize userNameString = _userNameString;
@synthesize userNameInitialsString = _userNameInitialsString;
@synthesize userStatus = _userStatus;
@synthesize userImage = _userImage;
@synthesize userBackgroundColor = _userBackgroundColor;

-(NSString *)getInitialsOfName:(NSString *)nameString{
    NSMutableString * firstCharacters = [NSMutableString string];
    NSArray * words = [nameString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    int i=0;
    for (NSString * word in words) {
        if (i<2) {
            if ([word length] > 0) {
                NSString * firstLetter = [word substringToIndex:1];
                [firstCharacters appendString:[firstLetter uppercaseString]];
                i+=1;
            }
        }
    }
    return firstCharacters;
}

// A method to convert string to enum
-(kStatus) statusFromString:(NSString*_Nonnull)userStatusString{
    NSArray *featureTypeArray = [[NSArray alloc] initWithObjects:kStatusArray];
    NSUInteger n = [featureTypeArray indexOfObject:userStatusString];
    return (kStatus) n;
}

-(id _Nonnull )initWithUserName:(NSString *)userName andUserStatus:(NSString *)userStatus andUserImage:(UIImage *)userImage orUserColor:(NSString *)userColor{
    self =[super init];
    if (self) {
        _userNameString = userName;
        _userNameInitialsString = [self getInitialsOfName:userName];
        _userStatus = [self statusFromString:userStatus];
        _userImage = userImage;
        _userBackgroundColor = userColor;
    }
    return self;
}

@end
