//
//  ParticipantView.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 07/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "ParticipantView.h"
#import "Constants.h"

@implementation ParticipantView

@synthesize participantLayoutView = _participantLayoutView;
@synthesize participantNameLabel = _participantNameLabel;
@synthesize availabilityStatusView = _availabilityStatusView;

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self createParticipantViewWithInstance:self];
    }
    return self;
}

-(void)createParticipantViewWithInstance:(ParticipantView *)participantView{
    _participantLayoutView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.participantLayoutView.layer.cornerRadius = self.participantLayoutView.frame.size.width / 2;
    self.participantLayoutView.clipsToBounds = YES;
    [self.participantLayoutView setBackgroundColor:[UIColor cyanColor]];
    [self addSubview:_participantLayoutView];
    
    _participantNameLabel = [[UILabel alloc] init];
    [_participantNameLabel setFont:[UIFont systemFontOfSize:11.0f]];
    [_participantNameLabel setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:_participantNameLabel];
    
    _availabilityStatusView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width-X_OFFSET/2, self.frame.size.height-VIEW_SIZE/3-Y_MARGIN, VIEW_SIZE/3, VIEW_SIZE/3)];
    self.availabilityStatusView.layer.cornerRadius = self.availabilityStatusView.frame.size.width / 2;
    self.availabilityStatusView.clipsToBounds = YES;
    [self.availabilityStatusView setBackgroundColor:[UIColor orangeColor]];
    [self addSubview:_availabilityStatusView];
}

@end
