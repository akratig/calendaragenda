//
//  ScrollerHandler.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CalendarDelegate.h"

#import "MonthYearViewObject.h"
#import "ScrollViewObject.h"

// Synchronize ContentView and MonthYearView

@interface ScrollerHandler : NSObject

@property (nonatomic, weak) CalendarHandler *calendarHandler;

@property (nonatomic, weak) UIView<MonthYearViewObject> *monthYearView;
@property (nonatomic, weak) UIScrollView<ScrollViewObject> *contentView;

- (void)setMenuPrevDate:(NSDate *)previousDate
                todayDate:(NSDate *)todayDate
                   nextDate:(NSDate *)nextDate;

- (void)modifyMenuContentOffset:(CGFloat)percentage calViewMode:(NSUInteger)calViewMode;
- (void)modifyContentOffset:(CGFloat)percentage;

@end
