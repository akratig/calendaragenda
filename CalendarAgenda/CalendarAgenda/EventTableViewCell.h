//
//  EventTableViewCell.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 28/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllParticipantsView.h"

@interface EventTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *timeDurationLabel;
@property (nonatomic, strong) UIView *eventTypeLayoutView;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) AllParticipantsView *allParticipantsView;
@property (nonatomic, strong) UIImageView *locationImageView;
@property (nonatomic, strong) UILabel *meetingPlaceLabel;

@end
