//
//  AllEventsData.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 07/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "AllEventsData.h"
#import "EventData.h"
#import "DateTimeManager.h"
#import "ParticipantData.h"
#import "CommonMethods.h"

@implementation AllEventsData

- (instancetype)init
{
    self = [super init];
    if(!self){
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

// For overriding
- (void)commonInit
{
    
}

// Setting Fake data for events
- (NSMutableDictionary *)createRandomEvents
{
    NSMutableDictionary *_eventsDictionary = [NSMutableDictionary new];
    
    for(int i = 0; i < 30; ++i){
        // Generate 30 random dates for events between now and 60 days later
        NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
        // Use the date as key for eventsDictionary
        NSString *key = [DateTimeManager dateStringWithFormat:@"dd-MM-yyyy" andDate:randomDate];
        if(!_eventsDictionary[key]){
            _eventsDictionary[key] = [NSMutableArray new];
        }
        
        //Set event details data here
        EventData *eventData = [[EventData alloc]init];
        [eventData setEventDate:randomDate];
        [eventData setTimeString:[DateTimeManager getTimeFromDate:randomDate]];
        
        NSArray *timeDurationArray = [NSArray arrayWithObjects: @"1h 30m", @"15m", @"1h", nil];
        NSArray *descriptionArray = [NSArray arrayWithObjects: @"Free Lunch & Speechless Madness", @"Team Lunch", @"Surprise Birthday Party", @"Outlook Mobile All Hands", nil];
        NSArray *participantNamesArray = [NSArray arrayWithObjects: @"Puneetha KS", @"Ogden Kent", @"Akrati Goel", @"	Samirul Mallick", nil];
        NSArray *participantStatusArray = [NSArray arrayWithObjects: @"AVAILABLE", @"BUSY", @"IDLE", @"	OFFLINE", nil];
        NSArray *meetingPlaceArray = [NSArray arrayWithObjects: @"Mountain View, SVC-1 MPRs", @"Alameda", @"Gasoline Coffee Alley", nil];
        NSArray *backgroundColorArray = [NSArray arrayWithObjects: @"LIGHT_GREEN_COLOR", @"LIGHT_PURPLE_COLOR", @"MUSTARD_COLOR", @"GREEN_COLOR", nil];
        
        [eventData setTimeDurationString:[CommonMethods getRandomStringFromArray:timeDurationArray]];
        [eventData setDescriptionString:[CommonMethods getRandomStringFromArray:descriptionArray]];
        NSString *userStatusString;
        eventData.participantsArray = [[NSMutableArray alloc]init];
        [eventData.participantsArray removeAllObjects];
        
        NSArray *finalNamesArray = [CommonMethods getRandomArrayFromArray:participantNamesArray];
        NSArray *finalColorArray = [CommonMethods getRandomArrayFromArray:backgroundColorArray];
        
        //Taking 3 participants as of now, case for dynamic participants not handled
        for (int i=0; i<3; i++) {
            userStatusString = [CommonMethods getRandomStringFromArray:participantStatusArray];
            ParticipantData *participantData = [[ParticipantData alloc]initWithUserName:[finalNamesArray objectAtIndex:i] andUserStatus:userStatusString andUserImage:[UIImage imageNamed:[CommonMethods getRandomStringFromArray:backgroundColorArray]] orUserColor:[finalColorArray objectAtIndex:i]];
            
            [eventData.participantsArray addObject:participantData];
        }
        [eventData setMeetingPlaceString:[CommonMethods getRandomStringFromArray:meetingPlaceArray]];
        
        [_eventsDictionary[key] addObject:eventData];
    }
    return _eventsDictionary;
}

@end
