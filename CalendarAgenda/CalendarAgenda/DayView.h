//
//  DayView.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DayViewObject.h"

@interface DayView : UIView<DayViewObject>

@property (nonatomic, weak) CalendarHandler *calendarHandler;

@property (nonatomic) NSDate *date;

@property (nonatomic, readonly) UIView *circleView;
@property (nonatomic, readonly) UIView *dotView;
@property (nonatomic, readonly) UILabel *monthLabel;
@property (nonatomic, readonly) UILabel *dateLabel;

@property (nonatomic) CGFloat circleRatio;
@property (nonatomic) CGFloat dotRatio;

@property (nonatomic) BOOL isFromAnotherMonth;

- (void)commonInit;

@end
