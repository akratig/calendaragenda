//
//  AllParticipantsView.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 07/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllParticipantsView : UIView

- (id)initWithFrame:(CGRect)frame;
-(id)updateWithParticipants:(NSMutableArray *)allParticipantsArray;

@end
