//
//  ServerHandler.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 03/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SERVER_URL [NSURL URLWithString:@"https://api.darksky.net/forecast/"]
#define SECRET_KEY "38386a017458c5e5ae2752a474d59b3c"

@interface ServerHandler : NSObject{
    NSUInteger state;
}

@property(assign)NSUInteger state;

-(void)getCurrentTemperature;

@end
