//
//  LayoutView.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CalendarViewObject.h"

@interface LayoutView : UIView<CalendarViewObject>

@property (nonatomic, weak) CalendarHandler *calendarHandler;

@property (nonatomic) NSDate *date;

- (void)commonInit;

@end
