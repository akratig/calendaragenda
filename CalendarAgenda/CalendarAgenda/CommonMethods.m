//
//  CommonMethods.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 08/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "CommonMethods.h"
#import "Constants.h"

@implementation CommonMethods


+(UIColor *)findColorWithString:(NSString *)userBackgroundColor{
    UIColor *theColor;
    if ([userBackgroundColor isEqualToString:@"LIGHT_GREEN_COLOR"]) {
        theColor = LIGHT_GREEN_COLOR;
    }
    
    if ([userBackgroundColor isEqualToString:@"LIGHT_PURPLE_COLOR"]) {
        theColor = LIGHT_PURPLE_COLOR;
    }
    
    if ([userBackgroundColor isEqualToString:@"MUSTARD_COLOR"]) {
        theColor = MUSTARD_COLOR;
    }
    
    if ([userBackgroundColor isEqualToString:@"GREEN_COLOR"]) {
        theColor = GREEN_COLOR;
    }
    
    return theColor;
}

+(NSArray *)getRandomArrayFromArray:(NSArray *)array{
    NSMutableArray *initialArray = [[NSMutableArray alloc]initWithArray:array];
    NSMutableArray *randomStringsArray = [[NSMutableArray alloc] init];
    NSInteger randomIndex;
    NSString *randomObject;
    // Taking only for three participants as of now
    for (int i=0; i<3; i++) {
        randomIndex = arc4random()%[initialArray count];
        randomObject = [initialArray objectAtIndex:randomIndex];
        [initialArray removeObjectAtIndex:randomIndex];
        [randomStringsArray addObject:randomObject];
    }
    
    return randomStringsArray;
}

+(NSString *)getRandomStringFromArray:(NSArray *)array{
    NSInteger randomIndex = arc4random()%[array count];
    NSString *randomObject = [array objectAtIndex:randomIndex];
    return randomObject;
}

@end
