//
//  CalendarDelegate.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CalendarViewObject.h"
#import "WeekViewObject.h"
#import "WeekDayViewObject.h"
#import "DayViewObject.h"

@class CalendarHandler;

@protocol CalendarDelegate <NSObject>

@optional

// Month Year view

//Returns a UIView, for the monthYearView, by default, it returns UILabel

- (UIView *)createMonthYearView:(CalendarHandler *)calendarHandler;

//For customizing the monthYearItemView

- (void)calendarHandler:(CalendarHandler *)calendarHandler prepareMonthYearView:(UIView *)monthYearItemView date:(NSDate *)date;

// Content view

//Checks if the cal can go to this date, returns `YES` by default

- (BOOL)calendarHandler:(CalendarHandler *)calendarHandler canDisplayPageWithDate:(NSDate *)date;

//Returns the date for the previous page. By default, returns 1 month before the current date

- (NSDate *)calendarHandler:(CalendarHandler *)calendarHandler dateForPrevPageWithCurrentDate:(NSDate *)todayDate;

//Returns the date for the next page. By default, returns 1 month after the current date

- (NSDate *)calendarHandler:(CalendarHandler *)calendarHandler dateForNextPageWithCurrentDate:(NSDate *)todayDate;

//Called when previous page becomes the current page.

- (void)previousPageLoaded:(CalendarHandler *)calendarHandler;

//Called when next page becomes the current page.

- (void)nextPageLoaded:(CalendarHandler *)calendarHandler;

//Returns a view conforming to `CalendarViewObject` protocol. By default, returns an instance of `LayoutView`
- (UIView<CalendarViewObject> *)createLayoutView:(CalendarHandler *)calendarHandler;

// Calendar view

//Returns a view conforming to `WeekDayViewObject` protocol. By default, returns an instance of `WeekDayView`
- (UIView<WeekDayViewObject> *)createWeekDayView:(CalendarHandler *)calendarHandler;

//Returns a view conforming to `WeekViewObject` protocol. By default, returns an instance of `WeekView` .
- (UIView<WeekViewObject> *)createWeekView:(CalendarHandler *)calendarHandler;

// Week view

//Returns a view conforming to `DayViewObject` protocol. By default, returns an instance of `DayView` .

- (UIView<DayViewObject> *)createDayView:(CalendarHandler *)calendarHandler;

// Day view

//customize the dayView.

- (void)calendarHandler:(CalendarHandler *)calendarHandler prepareDayView:(UIView<DayViewObject> *)dayView;

// dayView touched.

- (void)calendarHandler:(CalendarHandler *)calendarHandler didTouchDayView:(UIView<DayViewObject> *)dayView;

@end
