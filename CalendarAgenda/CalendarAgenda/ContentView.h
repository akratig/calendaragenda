//
//  ContentView.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ScrollViewObject.h"

@interface ContentView : UIScrollView<ScrollViewObject>

@property (nonatomic, weak) CalendarHandler *calendarHandler;

@property (nonatomic) NSDate *date;

- (void)commonInit;

@end
