//
//  WeekDayViewObject.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CalendarHandler;

@protocol WeekDayViewObject <NSObject>

- (void)setCalendarHandler:(CalendarHandler *)calendarHandlerHandler;

- (void)reload;

@end
