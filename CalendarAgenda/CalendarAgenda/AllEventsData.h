//
//  AllEventsData.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 07/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AllEventsData : NSObject

- (NSMutableDictionary *)createRandomEvents;

@end
