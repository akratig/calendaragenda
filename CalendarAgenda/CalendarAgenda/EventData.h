//
//  EventData.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 02/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventData : NSObject

@property (nonatomic, strong) NSDate *eventDate;
@property (nonatomic, strong) NSString *timeString;
@property (nonatomic, strong) NSString *timeDurationString;
@property (nonatomic, strong) NSString *descriptionString;
@property (nonatomic, strong) NSString *participantNameString;
@property (nonatomic, strong) NSMutableArray *participantsArray;
@property (nonatomic, strong) NSString *meetingPlaceString;

@end
