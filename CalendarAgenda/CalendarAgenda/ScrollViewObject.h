//
//  Content.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CalendarHandler;

@protocol ScrollViewObject <NSObject>

- (void)setCalendarHandler:(CalendarHandler *)calendarHandlerHandler;

- (NSDate *)date;
- (void)setDate:(NSDate *)date;

- (void)loadPrevPage;
- (void)loadNextPage;

- (void)loadPrevPageWithAnimation;
- (void)loadNextPageWithAnimation;

@end
