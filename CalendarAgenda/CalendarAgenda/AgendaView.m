//
//  AgendaView.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 28/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "AgendaView.h"
#import "EventTableViewCell.h"
#import "EventData.h"
#import "Constants.h"
#import "CommonMethods.h"
#import "DateTimeManager.h"

@implementation AgendaView

@synthesize tableView;

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self createEventsTableWithInstance:self];
    }
    return self;
}

-(void)createEventsTableWithInstance:(AgendaView *)agendaView{
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, agendaView.frame.size.width, agendaView.frame.size.height) style:UITableViewStylePlain];
    [tableView setDataSource:self];
    [tableView setDelegate:self];
    [tableView reloadData];
    [self addSubview:tableView];
}

// Returns the date string in required format
-(NSString *)dateStringWithFormat:(NSString *)dateFormat andDate:(NSDate *)date
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = dateFormat;
    }
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

-(void)updateViewWithData:(NSMutableArray *)eventsDataArray forDay:(NSDate *)date{
    eventsArray = [[NSMutableArray alloc]initWithArray:eventsDataArray];
    headerString = [[self dateStringWithFormat:@"EEEE, MMMM dd" andDate:date] uppercaseString];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tblView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"EventTableViewCell";
    
    EventTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[EventTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: cellIdentifier];
    }
    
    // Set up the cell…
    EventData *eventData = [[EventData alloc]init];
    eventData = [eventsArray objectAtIndex:indexPath.row];
    
    [cell.timeLabel setText:eventData.timeString];
    [cell.timeDurationLabel setText:eventData.timeDurationString];
    
    NSArray *backgroundColorArray = [NSArray arrayWithObjects: @"LIGHT_GREEN_COLOR", @"LIGHT_PURPLE_COLOR", @"MUSTARD_COLOR", @"GREEN_COLOR", nil];
    NSString *randomColorString = [CommonMethods getRandomStringFromArray:backgroundColorArray];
    [cell.eventTypeLayoutView setBackgroundColor:[CommonMethods findColorWithString:randomColorString]];
    
    cell.descriptionLabel.text = eventData.descriptionString;
    
    cell.descriptionLabel.numberOfLines = 0;
    CGFloat labelWidth = cell.frame.size.width-(cell.eventTypeLayoutView.frame.origin.x+cell.eventTypeLayoutView.frame.size.width+X_OFFSET);
    CGSize maximumLabelSize = CGSizeMake(labelWidth, MAXFLOAT);
    CGSize expectedSize = [cell.descriptionLabel sizeThatFits:maximumLabelSize];
    [cell.descriptionLabel setFrame:CGRectMake(cell.descriptionLabel.frame.origin.x, cell.descriptionLabel.frame.origin.y, expectedSize.width, expectedSize.height)];
    
    NSArray *subViewsArray = [[cell allParticipantsView] subviews];
    for (id subView in subViewsArray) {
        [subView removeFromSuperview];
    }
    [[cell allParticipantsView] setFrame:CGRectMake(cell.descriptionLabel.frame.origin.x, cell.descriptionLabel.frame.origin.y+cell.descriptionLabel.frame.size.height+Y_MARGIN, VIEW_SIZE, VIEW_SIZE)];
    [cell setAllParticipantsView:[cell.allParticipantsView updateWithParticipants:eventData.participantsArray]];
    [[cell locationImageView] setFrame:CGRectMake(cell.descriptionLabel.frame.origin.x, cell.allParticipantsView.frame.origin.y+cell.allParticipantsView.frame.size.height+Y_OFFSET, [cell locationImageView].frame.size.width, [cell locationImageView].frame.size.height)];
    [[cell meetingPlaceLabel] setFrame:CGRectMake(cell.locationImageView.frame.origin.x+cell.locationImageView.frame.size.width+X_MARGIN, cell.locationImageView.frame.origin.y+Y_MARGIN, labelWidth-cell.locationImageView.frame.size.width-X_MARGIN*2, LABEL_HEIGHT)];
    [cell.meetingPlaceLabel setText:eventData.meetingPlaceString];
    
    cellHeight = cell.meetingPlaceLabel.frame.origin.y+cell.meetingPlaceLabel.frame.size.height+Y_OFFSET;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [eventsArray count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, LABEL_HEIGHT+Y_MARGIN)];
    headerView.backgroundColor = LIGHT_BLUE_COLOR;
    [[headerView layer] setBorderWidth:0.5f];
    [[headerView layer]setBorderColor:[[UIColor lightGrayColor] CGColor]];
    CGRect initialFrame = CGRectMake(0, 0, self.frame.size.width-X_OFFSET, LABEL_HEIGHT);
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(Y_MARGIN, X_OFFSET, 1, 1);
    CGRect paddedFrame = UIEdgeInsetsInsetRect(initialFrame, contentInsets);
    
    UILabel *label = [[UILabel alloc] initWithFrame:paddedFrame];
    label.font = [UIFont systemFontOfSize:14];
    label.text = headerString;
    [label setTextColor: NORMAL_BLUE_COLOR];
    [headerView addSubview:label];
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return LABEL_HEIGHT+Y_MARGIN;
}

- (void)tableView:(UITableView *)tblView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
