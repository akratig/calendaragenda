//
//  DayViewObject.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CalendarHandler;

@protocol DayViewObject <NSObject>

- (void)setCalendarHandler:(CalendarHandler *)calendarHandlerHandler;

- (NSDate *)date;
- (void)setDate:(NSDate *)date;

- (BOOL)isFromAnotherMonth;
- (void)setIsFromAnotherMonth:(BOOL)isFromAnotherMonth;

@end
