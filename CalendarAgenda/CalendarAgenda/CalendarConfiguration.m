//
//  CalendarConfiguration.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "CalendarConfiguration.h"

@implementation CalendarConfiguration

- (instancetype)init
{
    self = [super init];
    if(!self){
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

// For overriding
- (void)commonInit
{
    _calViewHideWhenPossible = NO;
    _calViewNumberOfWeeks = 6;
    _calViewHaveWeekDaysView = YES;
    _calViewWeekDaysViewAutomaticHeight = NO;
    _calViewWeekModeNumberOfWeeks = 1;
}

@end
