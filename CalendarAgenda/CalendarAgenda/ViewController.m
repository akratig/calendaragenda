//
//  ViewController.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 23/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "ViewController.h"
#import "CalendarHandler.h"
#import "DayView.h"
#import "EventData.h"
#import "Constants.h"
#import "ServerHandler.h"
#import "CustomBarButtonItem.h"
#import "AllEventsData.h"

@interface ViewController (){
    NSMutableDictionary *_eventsDictionary;
    
    NSDate *_todayDate;
    NSDate *_minDate;
    NSDate *_maxDate;
    NSDate *_dateSelected;
}

@end

@implementation ViewController

-(void)menuAction{
    //perform menu action here..
}

-(void)rightMenuAction{
    //perform menu action here..
}

-(void)addAction{
    //perform add action here..
}

-(void)temperatureAction{
    NSString *summaryStr= [[temperatureDataDict objectForKey:@"currently"] objectForKey:@"summary"];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Weather report"
                                  message:summaryStr
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                         }];
    [alert addAction:ok]; // add action to uialertcontroller
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)dataNotif:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TemperatureNotification" object:nil];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = FALSE;
    if ([notification object]!=nil) {
        temperatureDataDict = [[NSDictionary alloc]initWithDictionary:[notification object]];
        
        NSString *fahrenheitTempStr= [[temperatureDataDict objectForKey:@"currently"] objectForKey:@"temperature"];
        float fahrenheitTemp = [fahrenheitTempStr floatValue];
        
        //Converting temperature from fahrenheit to celsius
        
        float  celsiusTemp= (fahrenheitTemp-32)*5/9;
        int tempInCel = (int)celsiusTemp;
        NSString *celsiusTempStr = [NSString stringWithFormat:@"%d °C",tempInCel];
        [self setNavBarWithTemperature:celsiusTempStr];
    }
}

-(void)getTemperatureForCurrentLocation{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    ServerHandler *connObj;
    connObj = [[ServerHandler alloc]init];
    connObj.state = 1;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataNotif:) name:@"TemperatureNotification" object:nil];
    [connObj getCurrentTemperature];

}

-(void)setNavBarWithoutTemperature{
    UIBarButtonItem *leftBarButton = [[CustomBarButtonItem alloc] initWithImage:MENU_IMAGE addTarget:self action:@selector(menuAction)];
    self.navigationItem.leftBarButtonItem = leftBarButton;
}

-(void)setNavBarWithTemperature:(NSString *)temperature{
    UIBarButtonItem *menuBarButton = [[CustomBarButtonItem alloc] initWithImage:MENU_IMAGE addTarget:self action:@selector(menuAction)];
    
    UIBarButtonItem *temperatureBarButton = [[UIBarButtonItem alloc] initWithTitle:temperature style:UIBarButtonItemStyleDone target:self action:@selector(temperatureAction)];
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:menuBarButton, temperatureBarButton,nil]];
}

-(void)createBasicUI{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    [self setNavBarWithoutTemperature];
    UIBarButtonItem *rightMenuBarButton = [[CustomBarButtonItem alloc] initWithImage:OPTIONS_IMAGE addTarget:self action:@selector(rightMenuAction)];
    UIBarButtonItem *addBarButton = [[CustomBarButtonItem alloc] initWithImage:ADD_IMAGE addTarget:self action:@selector(addAction)];
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:addBarButton, rightMenuBarButton,nil]];
    monthYearView = [[MonthYearView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.navigationController.navigationBar.frame.size.height)];
    [self.navigationItem setTitleView:monthYearView];
    
    contentView = [[ContentView alloc]initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, self.view.frame.size.width, 300)];
    [self.view addSubview:contentView];
    
    agendaView = [[AgendaView alloc]initWithFrame:CGRectMake(0, contentView.frame.origin.y+contentView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-(contentView.frame.origin.y+contentView.frame.size.height))];
    [self.view addSubview:agendaView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _calendarHandler = [CalendarHandler new];
    _calendarHandler.delegate = self;
    
    [self getTemperatureForCurrentLocation];

    // Generate random events sort by date using a dateformatter for the demonstration
    [self createRandomEvents];
    
    // Create a min and max date for limit the cal, optional
    [self setCalendarBounds];
    
    [self createBasicUI];
    
    [_calendarHandler setMonthYearView:monthYearView];
    [_calendarHandler setContentView:contentView];
    [_calendarHandler setDate:_todayDate];
}

#pragma mark - CalendarHandler delegate

// Customizing the appearance of dayView
- (void)calendarHandler:(CalendarHandler *)calendarHandler prepareDayView:(DayView *)dayView
{
    // Today's Date
    if([_calendarHandler.dateTimeManager date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = NORMAL_BLUE_COLOR;
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.dateLabel.textColor = [UIColor whiteColor];
        dayView.monthLabel.textColor = [UIColor whiteColor];
        
        //update events table with Today's events
        
        NSString *key = [DateTimeManager dateStringWithFormat:@"dd-MM-yyyy" andDate:dayView.date];
        [self updateEventsTableForDate:dayView.date withKey:key];
    }
    // Selected day
    else if(_dateSelected && [_calendarHandler.dateTimeManager date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = LIGHT_BLUE_COLOR;
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.monthLabel.textColor = [UIColor blackColor];
        dayView.dateLabel.textColor = [UIColor blackColor];
    }
    // Different month
    else if(![_calendarHandler.dateTimeManager date:contentView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = NORMAL_BLUE_COLOR;
        dayView.monthLabel.textColor = [UIColor lightGrayColor];
        dayView.dateLabel.textColor = [UIColor lightGrayColor];
    }
    // Different day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = NORMAL_BLUE_COLOR;
        dayView.dateLabel.textColor = [UIColor blackColor];
        dayView.monthLabel.textColor = [UIColor blackColor];
    }
    
    if([self haveEventForDay:dayView.date]){
        dayView.dotView.hidden = NO;
    }
    else{
        dayView.dotView.hidden = YES;
    }
}

- (void)calendarHandler:(CalendarHandler *)calendarHandler didTouchDayView:(DayView *)dayView
{
    _dateSelected = dayView.date;
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView duration:.3 options:0 animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarHandler reload];
                    } completion:nil];
    
    // Load the previous or next page if touch a day from another month
    if(![_calendarHandler.dateTimeManager date:contentView.date isTheSameMonthThan:dayView.date]){
        if([contentView.date compare:dayView.date] == NSOrderedAscending){
            [contentView loadNextPageWithAnimation];
        }
        else{
            [contentView loadPrevPageWithAnimation];
        }
    }
    
    //update events table
    NSString *key = [DateTimeManager dateStringWithFormat:@"dd-MM-yyyy" andDate:dayView.date];
    
    [self updateEventsTableForDate:dayView.date withKey:key];
}

#pragma mark - CalendarHandler delegate - Calendar Views mangement

// Used to limit the date for the cal, optional
- (BOOL)calendarHandler:(CalendarHandler *)calendarHandler canDisplayPageWithDate:(NSDate *)date
{
    return [_calendarHandler.dateTimeManager date:date isEqualOrAfter:_minDate andEqualOrBefore:_maxDate];
}

- (void)nextPageLoaded:(CalendarHandler *)calendarHandler
{
    // Next page loaded
}

- (void)previousPageLoaded:(CalendarHandler *)calendarHandler
{
    // Previous page loaded
}

#pragma mark - Fake data

- (void)setCalendarBounds
{
    _todayDate = [NSDate date];
    
    // Min date will be 12 month before today
    _minDate = [_calendarHandler.dateTimeManager addToDate:_todayDate months:-12];
    
    // Max date will be 12 months after today
    _maxDate = [_calendarHandler.dateTimeManager addToDate:_todayDate months:12];
}

-(void)updateEventsTableForDate:(NSDate *)date withKey:(NSString *)dateKey{
    NSMutableArray *allEvents = [_eventsDictionary objectForKey:dateKey];
    [agendaView updateViewWithData:allEvents forDay:date];
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    NSString *key = [DateTimeManager dateStringWithFormat:@"dd-MM-yyyy" andDate:date];
    
    if(_eventsDictionary[key] && [_eventsDictionary[key] count] > 0){
        return YES;
    }
    
    return NO;
}

- (void)createRandomEvents
{
    _eventsDictionary = [NSMutableDictionary new];
    
    //Get all fake random data for events
    AllEventsData *allEventsData = [[AllEventsData alloc] init];
    _eventsDictionary = [NSMutableDictionary dictionaryWithDictionary:[allEventsData createRandomEvents]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
