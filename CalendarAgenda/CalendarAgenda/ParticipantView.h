//
//  ParticipantView.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 07/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParticipantView : UIView

@property (nonatomic, strong) UIImageView *participantLayoutView;
@property (nonatomic, strong) UILabel *participantNameLabel;
@property (nonatomic, strong) UIView *availabilityStatusView;

- (id)initWithFrame:(CGRect)frame;

@end
