//
//  CalendarDelegateHandler.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "CalendarDelegateHandler.h"

#import "CalendarHandler.h"

#import "LayoutView.h"
#import "WeekDayView.h"
#import "WeekView.h"
#import "DayView.h"
#import "Constants.h"

@implementation CalendarDelegateHandler

#pragma mark - MonthYear view

- (UIView *)buildMonthYearView
{
    if(_calendarHandler.delegate && [_calendarHandler.delegate respondsToSelector:@selector(createMonthYearView:)]){
        return [_calendarHandler.delegate createMonthYearView:self.calendarHandler];
    }
    
    UILabel *label = [UILabel new];
    label.textAlignment = NSTextAlignmentCenter;
    
    return label;
}

- (void)prepareMonthYearView:(UIView *)monthYearView date:(NSDate *)date
{
    if(_calendarHandler.delegate && [_calendarHandler.delegate respondsToSelector:@selector(calendarHandler:prepareMonthYearView:date:)]){
        [_calendarHandler.delegate calendarHandler:self.calendarHandler prepareMonthYearView:monthYearView date:date];
        return;
    }
    
    NSString *text = nil;
    
    if(date){
        NSCalendar *cal = _calendarHandler.dateTimeManager.cal;
        NSDateComponents *comps = [cal components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:date];
        NSInteger currentMonthIndex = comps.month;
        NSInteger currentYear = comps.year;
        
        static NSDateFormatter *dateFormatter = nil;
        if(!dateFormatter){
            dateFormatter = [_calendarHandler.dateTimeManager createDateFormatter];
        }
        
        dateFormatter.timeZone = _calendarHandler.dateTimeManager.cal.timeZone;
        dateFormatter.locale = _calendarHandler.dateTimeManager.cal.locale;
        
        while(currentMonthIndex <= 0){
            currentMonthIndex += 12;
        }
        
        text = [[dateFormatter standaloneMonthSymbols][currentMonthIndex - 1] capitalizedString];
        text = [NSString stringWithFormat:@"%@ %ld",text,currentYear];
    }
    [(UILabel *)monthYearView setTextColor:NORMAL_BLUE_COLOR];
    [(UILabel *)monthYearView setText:text];
}

#pragma mark - Content view

- (UIView<CalendarViewObject> *)buildLayoutView
{
    if(_calendarHandler.delegate && [_calendarHandler.delegate respondsToSelector:@selector(createLayoutView:)]){
        return [_calendarHandler.delegate createLayoutView:self.calendarHandler];
    }
    
    return [LayoutView new];
}

- (BOOL)canDisplayPageWithDate:(NSDate *)date
{
    if(_calendarHandler.delegate && [_calendarHandler.delegate respondsToSelector:@selector(calendarHandler:canDisplayPageWithDate:)]){
        return [_calendarHandler.delegate calendarHandler:self.calendarHandler canDisplayPageWithDate:date];
    }
    
    return YES;
}

- (NSDate *)dateForPrevPageWithCurrentDate:(NSDate *)todayDate
{
    NSAssert(todayDate != nil, @"todayDate cannot be nil");
    
    if(_calendarHandler.delegate && [_calendarHandler.delegate respondsToSelector:@selector(calendarHandler:dateForPrevPageWithCurrentDate:)]){
        return [_calendarHandler.delegate calendarHandler:self.calendarHandler dateForPrevPageWithCurrentDate:todayDate];
    }
    
    return [_calendarHandler.dateTimeManager addToDate:todayDate months:-1];
}

- (NSDate *)dateForNextPageWithCurrentDate:(NSDate *)todayDate
{
    NSAssert(todayDate != nil, @"todayDate cannot be nil");
    
    if(_calendarHandler.delegate && [_calendarHandler.delegate respondsToSelector:@selector(calendarHandler:dateForNextPageWithCurrentDate:)]){
        return [_calendarHandler.delegate calendarHandler:self.calendarHandler dateForNextPageWithCurrentDate:todayDate];
    }
    
    return [_calendarHandler.dateTimeManager addToDate:todayDate months:1];
}

#pragma mark - Calendar view

- (UIView<WeekDayViewObject> *)buildWeekDayView
{
    if(_calendarHandler.delegate && [_calendarHandler.delegate respondsToSelector:@selector(createWeekDayView:)]){
        return [_calendarHandler.delegate createWeekDayView:self.calendarHandler];
    }
    
    return [WeekDayView new];
}

- (UIView<WeekViewObject> *)buildWeekView
{
    if(_calendarHandler.delegate && [_calendarHandler.delegate respondsToSelector:@selector(createWeekView:)]){
        return [_calendarHandler.delegate createWeekView:self.calendarHandler];
    }
    
    return [WeekView new];
}

#pragma mark - Week view

- (UIView<DayViewObject> *)buildDayView
{
    if(_calendarHandler.delegate && [_calendarHandler.delegate respondsToSelector:@selector(createDayView:)]){
        return [_calendarHandler.delegate createDayView:self.calendarHandler];
    }
    
    return [DayView new];
}

#pragma mark - Day view

- (void)prepareDayView:(UIView<DayViewObject> *)dayView
{
    if(_calendarHandler.delegate && [_calendarHandler.delegate respondsToSelector:@selector(calendarHandler:prepareDayView:)]){
        [_calendarHandler.delegate calendarHandler:self.calendarHandler prepareDayView:dayView];
    }
}

- (void)didTouchDayView:(UIView<DayViewObject> *)dayView
{
    if(_calendarHandler.delegate && [_calendarHandler.delegate respondsToSelector:@selector(calendarHandler:didTouchDayView:)]){
        [_calendarHandler.delegate calendarHandler:self.calendarHandler didTouchDayView:dayView];
    }
}

@end
