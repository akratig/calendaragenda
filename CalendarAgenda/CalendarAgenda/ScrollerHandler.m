//
//  ScrollerHandler.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "ScrollerHandler.h"

@implementation ScrollerHandler

- (void)setMenuPrevDate:(NSDate *)previousDate todayDate:(NSDate *)todayDate nextDate:(NSDate *)nextDate
{
    if(!_monthYearView){
        return;
    }
    
    [_monthYearView setPrevDate:previousDate todayDate:todayDate nextDate:nextDate];
}

- (void)modifyMenuContentOffset:(CGFloat)percentage calViewMode:(NSUInteger)calViewMode
{
    if(!_monthYearView){
        return;
    }
    
    [_monthYearView modifyContentLayout:calViewMode];
    _monthYearView.scrollView.contentOffset = CGPointMake(percentage * _monthYearView.scrollView.contentSize.width, 0);
}

- (void)modifyContentOffset:(CGFloat)percentage
{
    if(!_contentView){
        return;
    }
    
    _contentView.contentOffset = CGPointMake(percentage * _contentView.contentSize.width, 0);
}

@end
