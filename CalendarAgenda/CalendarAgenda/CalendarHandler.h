//
//  CalendarHandler.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CalendarDelegate.h"
#import "ScrollViewObject.h"
#import "MonthYearViewObject.h"
#import "DateTimeManager.h"
#import "CalendarConfiguration.h"
#import "CalendarDelegateHandler.h"
#import "ScrollerHandler.h"

@interface CalendarHandler : NSObject

@property (nonatomic, weak) id<CalendarDelegate> delegate;

@property (nonatomic, weak) UIView<MonthYearViewObject> *monthYearView;
@property (nonatomic, weak) UIScrollView<ScrollViewObject> *contentView;

@property (nonatomic, readonly) DateTimeManager *dateTimeManager;
@property (nonatomic, readonly) CalendarConfiguration *settings;

@property (nonatomic, readonly) CalendarDelegateHandler *delegateHandler;
@property (nonatomic, readonly) ScrollerHandler *scrollManager;

- (void)commonInit;

- (NSDate *)date;
- (void)setDate:(NSDate *)date;
- (void)reload;

@end
