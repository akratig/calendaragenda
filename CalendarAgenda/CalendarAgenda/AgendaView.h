//
//  AgendaView.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 28/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventData.h"

@interface AgendaView : UIView<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *eventsArray;
    NSString *headerString;
    CGFloat cellHeight;
}

- (id)initWithFrame:(CGRect)frame;
-(void)updateViewWithData:(NSMutableArray *)eventsDataArray forDay:(NSDate *)headerString;

@property(nonatomic,strong)UITableView *tableView;

@end
