//
//  MonthYearView.h
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 27/01/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MonthYearViewObject.h"

@interface MonthYearView : UIView<MonthYearViewObject, UIScrollViewDelegate>

@property (nonatomic, weak) CalendarHandler *calendarHandler;

@property (nonatomic) CGFloat contentRatio;

@property (nonatomic, readonly) UIScrollView *scrollView;

- (void)commonInit;

@end
