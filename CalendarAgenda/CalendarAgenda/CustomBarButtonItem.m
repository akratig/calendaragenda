//
//  CustomBarButtonItem.m
//  CalendarAgenda
//
//  Created by ANKIT GOEL on 05/02/18.
//  Copyright © 2018 MS. All rights reserved.
//

#import "CustomBarButtonItem.h"
#import "Constants.h"

@implementation CustomBarButtonItem

-(id _Nonnull )initWithImage:(UIImage *_Nonnull)img addTarget:(nullable id)target action:(SEL _Nonnull )action{
    self =[super init];
    if (self) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake( 0, 0, VIEW_SIZE, VIEW_SIZE );
        [button setImage:img forState:UIControlStateNormal];
        [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
        [self createCustomBarButtonItemWith:self andButton:button];
    }
    return self;
}

-(void)createCustomBarButtonItemWith:(CustomBarButtonItem *)customBarButtonItem andButton:(UIButton *)button{
    [(UIBarButtonItem *)customBarButtonItem setCustomView:button];
}

@end
