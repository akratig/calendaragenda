# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for ###

* iOS app that replicates the basic features of the Calendar and Agenda views on the Outlook iOS (iPhone) app
* Version 1.0
* Ref: https://itunes.apple.com/us/app/microsoft-outlook-email-calendar/id951937596?mt=8

### Technology used ###

* Using Objective C on Xcode Version 8.3.2

### About me ###

I am into mobile apps development since 2010
In my career, I have developed various categories of apps: Service-based business apps, e-commerce apps, entertainment apps.
I would like to share about my most learning experience:

Developing Snapdeal's iOS app(for both iPhone and iPad) was a very learning experience from many aspects (Technical, business, team management). I was the first in-house developer to join Snapdeal's iOS Apps Development Team (There was an Android Guy who joined a month before me). When Snapdeal thought of launching their mobile apps, they first outsourced their apps (for both iOS and Android) to another company. The other company developed the app and Snapdeal launched the same since they were in a hurry to publish app to compete in the market. But there were many negative feedbacks on the appstore that app was crashing on almost every functionality. Sad part for Snapdeal was that there was noone to evaluate the technicalities of the project that was developed by the other company. So Snapdeal thought of having their own in-house apps development team.

When I joined, I was having the whole chunk of code. When I started having a look, I found that there were many memory leaks and the technical architecture of the code was very poor. I informed the business team that its better to develop a fresh app than to work on this. But due to business reasons, there were time constraints and I was supposed to start with the same code. So I started with improvising the code and side-by-side interviewed and made a team of 3 Good iOS Developers. I first divided the code into particular sections (like Home Screen to show categorised listing of products, Search, Sorting and Filters, etc.), then assigned particular sections to resources and then we started with fixing memory leaks (since iOS' Automatic Reference Counting for memory management was launched after 3 months of my joining). We had very strict time deadlines and a LOT of things to be fixed. After stabilising the app with memory management, I and my team started with improvising the app architecture. And within 2 months we stabilised the complete app and then proceeded with new features development.